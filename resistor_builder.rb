class ResistorBuilder
  # @param :wanted_value <Float> The total value of the resistor you want
  # @param :available_values Array<Float> Array of the resistor values you have available
  # @param :maximum <Integer> Maximum of resistors can be used in a solution
  # @param :error_margin <Float> The allowed error margin for a match, as a percentage
  def initialize(wanted_value, available_values = [], maximum: 3, error_margin: 0.0)
    # ... write this ...
  end

  # Return a list of solutions
  #
  # @returns [ResistorBuilder] Array of Arrays in the following format:
  #                            [
  #                              [
  #                                <:series or :parallel>,
  #                                <array of values used>
  #                              ]
  #                            ]
  def solutions
    # ... write this ...
  end
end
